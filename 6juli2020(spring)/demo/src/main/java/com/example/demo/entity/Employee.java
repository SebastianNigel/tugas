package com.example.demo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Employee {
    @Id
    @GeneratedValue
    private Integer id;
    private String nama;
    private String address;

    @OneToMany(targetEntity = Salary.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "eid", referencedColumnName = "id")
    private List<Salary> salaries;

    public String getNama() {
        return nama;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

}