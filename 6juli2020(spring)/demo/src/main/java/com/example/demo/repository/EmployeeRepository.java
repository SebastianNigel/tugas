package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

import com.example.demo.dto.EmployeeResponse;
import com.example.demo.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee ,Integer>{

    @Query("SELECT new com.example.demo.dto.EmployeeResponse(a.nama, b.salary) FROM Employee a join a.salaries b")
    public List<EmployeeResponse> getJoinInformation();
}