package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class EmployeeResponse {
    private String nama;
    private int salary;

    public void test(String nama, int salary) {
        this.nama = nama;
        this.salary = salary;
    }

    public String getNama() {
        return nama;
    }
    public int getSalary() {
        return salary;
    }
    

}