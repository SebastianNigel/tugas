package com.example.demo.controller;

import com.example.demo.dto.EmployeeRequest;
import com.example.demo.dto.EmployeeResponse;
import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.repository.SalaryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class EmployeeDetailController {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private SalaryRepository salaryRepository;

    @PostMapping("/addEmployee")
        public Employee saveEmployee(@RequestBody EmployeeRequest request){
            return employeeRepository.save(request.getEmployee());
        }
    @GetMapping("/getEmployee")
    public  List<EmployeeResponse> getDetailEmployee() {
        return employeeRepository.getJoinInformation();
    }
    
    
    }

