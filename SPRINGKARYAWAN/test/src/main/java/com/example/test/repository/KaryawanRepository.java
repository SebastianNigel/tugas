package com.example.test.repository;

import com.example.test.entity.Karyawan1;

import org.springframework.data.jpa.repository.JpaRepository;

public interface KaryawanRepository extends JpaRepository<Karyawan1, Integer> {
    Karyawan1 findByName(String name);
}