package com.example.test.service;

import java.util.List;

import com.example.test.entity.Karyawan1;
import com.example.test.repository.KaryawanRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KaryawanService {
    @Autowired
    private KaryawanRepository repository;

    public Karyawan1 saveKaryawan(Karyawan1 Karyawan) {
        return repository.save(Karyawan);
    }

    public List<Karyawan1> saveKaryawans(List<Karyawan1> Karyawans) {
        return repository.saveAll(Karyawans);
    }

    public List<Karyawan1> getKaryawans() {
        return repository.findAll();
    }

    public Karyawan1 getKaryawanById(int nik) {
        return repository.findById(nik).orElse(null);
    }

    public Karyawan1 getKaryawanByName(String name) {
        return repository.findByName(name);
    }

    public String deleteKaryawan(int id) {
        repository.deleteById(id);
        return "Karyawan Removed!";
    }

    public Karyawan1 updateKaryawan(Karyawan1 Karyawan) {
        Karyawan1 existingKaryawan = repository.findById(Karyawan.getNik()).orElse(null);
        existingKaryawan.setName(Karyawan.getName());
        existingKaryawan.setTempat_lahir(Karyawan.getTempat_lahir());
        existingKaryawan.setJeniskelamin(Karyawan.getJeniskelamin());
        existingKaryawan.setGolongan_darah(Karyawan.getGolongan_darah());
        existingKaryawan.setAlamat(Karyawan.getAlamat());
        existingKaryawan.setRtrw(Karyawan.getRtrw());
        existingKaryawan.setDesa_kelurahan(Karyawan.getDesa_kelurahan());
        existingKaryawan.setKecamatan(Karyawan.getKecamatan());
        existingKaryawan.setAgama(Karyawan.getAgama());
        existingKaryawan.setStatus_kawin(Karyawan.getStatus_kawin());
        existingKaryawan.setPekerjaan(Karyawan.getPekerjaan());
        existingKaryawan.setWarga(Karyawan.getWarga());
        return repository.save(existingKaryawan);
    }

}

