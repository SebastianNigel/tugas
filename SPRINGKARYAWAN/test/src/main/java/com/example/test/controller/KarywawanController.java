package com.example.test.controller;

import java.util.List;

import com.example.test.entity.Karyawan1;
import com.example.test.service.KaryawanService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class KarywawanController {
    @Autowired
    private KaryawanService service;

    @PostMapping("/addKaryawan")
    public Karyawan1 addKaryawan(@RequestBody Karyawan1 Karyawan) {
        return service.saveKaryawan(Karyawan);

    }

    @PostMapping("/addKaryawans")
    public List<Karyawan1> addKaryawans(@RequestBody List<Karyawan1> Karyawans) {
        return service.saveKaryawans(Karyawans);
    }

    @GetMapping("/karyawans")
    public List<Karyawan1> findAllKaryawans() {
        return service.getKaryawans();
    }

    @GetMapping("/Karyawan/{id}")
    public Karyawan1 findKaryawanByNik(@PathVariable int nik) {
        return service.getKaryawanById(nik);
    }

    @GetMapping("/KaryawanByName/{name}")
    public Karyawan1 findKaryawanByName(@PathVariable String name) {
        return service.getKaryawanByName(name);
    }

    @DeleteMapping("/deleteK/{nik}")
    public String deleteKaryawan(@PathVariable int nik) {
        return service.deleteKaryawan(nik);
    }

    @PutMapping("/update")
    public Karyawan1 updateKaryawan(@RequestBody Karyawan1 karyawan) {
        return service.saveKaryawan(karyawan);
    }

}   