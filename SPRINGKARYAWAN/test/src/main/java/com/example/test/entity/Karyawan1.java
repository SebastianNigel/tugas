package com.example.test.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "KARYAWAN_MASTER")
public class Karyawan1 {
    @Id
    private int nik;
    private String name;
    private String tempat_lahir;
    private String jeniskelamin;
    private String golongan_darah;
    private String alamat;
    private String rtrw;
    private String desa_kelurahan;
    private String kecamatan;
    private String agama;
    private String status_kawin;
    private String pekerjaan;
    private String warga;

    public int getNik() {
        return nik;
    }

    public String getGolongan_darah() {
        return golongan_darah;
    }

    public void setGolongan_darah(final String golongan_darah) {
        this.golongan_darah = golongan_darah;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(final String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getDesa_kelurahan() {
        return desa_kelurahan;
    }

    public void setDesa_kelurahan(final String desa_kelurahan) {
        this.desa_kelurahan = desa_kelurahan;
    }

    public String getRtrw() {
        return rtrw;
    }

    public void setRtrw(final String rtrw) {
        this.rtrw = rtrw;
    }

    public String getWarga() {
        return warga;
    }

    public void setWarga(final String warga) {
        this.warga = warga;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(final String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getStatus_kawin() {
        return status_kawin;
    }

    public void setStatus_kawin(final String status_kawin) {
        this.status_kawin = status_kawin;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(final String agama) {
        this.agama = agama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(final String alamat) {
        this.alamat = alamat;
    }

    public String getJeniskelamin() {
        return jeniskelamin;
    }

    public void setJeniskelamin(final String jeniskelamin) {
        this.jeniskelamin = jeniskelamin;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(final String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setNik(final int nik) {
        this.nik = nik;
    }

}
