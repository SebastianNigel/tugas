function addData() {
    let name = document.getElementById("nama").value;
    let tempat_lahir = document.getElementById("tempat-tgl-lahir").value;
    let jeniskelamin = document.getElementById("jenis_kelamin").value;
    let golongan_darah = document.getElementById("golongan_darah").value;
    let alamat = document.getElementById("Alamat").value;
    let rtrw = document.getElementById("rtrw").value;
    let desa_kelurahan = document.getElementById("Kelurahan").value;
    let kecamatan = document.getElementById("Kecamatan").value;
    let agama = document.getElementById("agama").value;
    let status_kawin = document.getElementById("status_kawin").value;
    let pekerjaan = document.getElementById("Pekerjaan").value;
    let warga = document.getElementById("warga").value;

    let jsonStr = JSON.stringify({
        name: name,
        tempat_lahir: tempat_lahir,
        jeniskelamin: jeniskelamin,
        golongan_darah: golongan_darah,
        alamat: alamat,
        rtrw: rtrw,
        desa_kelurahan: desa_kelurahan,
        kecamatan: kecamatan,
        agama: agama,
        status_kawin: status_kawin,
        pekerjaan: pekerjaan,
        warga: warga
    });

    fetch('http://localhost:1414/addKaryawan', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: jsonStr,
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            window.location.href = "index2.html"
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

function deleteData(nik) {
    let del = confirm("Yaqueen ingin menghapus data?")
    if (del) {
        fetch(`http://localhost:1414/deleteK/${nik}`, {
            method: 'DELETE',
        })
            .then(() => {
                window.alert("Data Telah Dihapus")
                location.reload()
            })
            .catch(console.error)
    }
}

function getData(url) {
    let http = new XMLHttpRequest();
    http.open("GET", url, true)
    http.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let response = JSON.parse(this.responseText)
            console.log(response.length)

            let msgReceipt = ""
            let no = 1;
            for (let i = 0; i < response.length; i++) {
                console.log(response[i]);
                msgReceipt += `
              <tr>
                  <td>${no}</td>
                  <td>${response[i].nik}</td>
                  <td>${response[i].name}</td>
                  <td>${response[i].tempat_lahir}</td>
                  <td>${response[i].jeniskelamin}</td>
                  <td>${response[i].golongan_darah}</td>
                  <td>${response[i].alamat}</td>
                  <td>${response[i].rtrw}</td>
                  <td>${response[i].desa_kelurahan}</td>
                  <td>${response[i].kecamatan}</td>
                  <td>${response[i].agama}</td>
                  <td>${response[i].status_kawin}</td>
                  <td>${response[i].pekerjaan}</td>
                  <td>${response[i].warga}</td>

                  
                  <td><!-- Button trigger modal -->
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#${response[i].name}Detail">
                  Detail
                  </button>
                  
                  <!-- Modal -->
                  <div class="modal fade" id="${response[i].name}Detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                        <form>
                        <div class="form-group">
                        <label style="font-weight : bold">Nik</label>
                        <br>
                        ${response[i].nik}
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Nama</label>
                        <br>
                        ${response[i].name}
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Tempat tanggal lahir</label>
                        <br>
                        ${response[i].tempat_lahir}
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Jenis Kelamin</label>
                        <br>
                        ${response[i].jeniskelamin}
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Golongan Darah</label>
                        <br>
                        ${response[i].golongan_darah}
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Alamat</label>
                        <br>
                        ${response[i].alamat}
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">RT/RW</label>
                        <br>
                        ${response[i].rtrw}
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Desa / Kelurahan</label>
                        <br>
                        ${response[i].desa_kelurahan}
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Kecamatan</label>
                        <br>
                        ${response[i].kecamatan}
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Agama</label>
                        <br>
                        ${response[i].agama} 
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Status pernikahan</label>
                        <br>
                        ${response[i].status_kawin} 
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Pekerjaan</label>
                        <br>
                        ${response[i].pekerjaan} 
                        </div>
                        <div class="form-group">
                        <label style="font-weight : bold">Kewarganegaraan</label>
                        <br>
                        ${response[i].warga}
                        </div>
                        </form>
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-secondary" data-dismiss="modal">Close</button>                
                          </div>
                          </div>
                          </div>
                          </div>
                          <button class="btn btn-danger" onclick="deleteData(${response[i].id})">Delete</button>
                  </td>
              </tr>
              `
                no++;
            }
            document.getElementById("tableBody").innerHTML = msgReceipt
        }
        //   function getItemForUpdate(id) {
        //     fetch(`http://localhost:1414/karyawans/${id}`)
        //         .then(response => response.json())
        //         .then(datas => editData(datas))
        //         .catch(console.error);
        //   }
        //   function updateData() {
        //     var nik = document.getElementById("edit-nik").value
        //     var nama = document.getElementById("edit-nama").value
        //     var tempat_lahir = document.getElementById("edit-tempat-lahir").value
        //     var jeniskelamin = document.getElementById("edit-jenis-kelamin").value
        //     var alamat = document.getElementById("edit-alamat").value
        //     var rtrw = document.getElementById("edit-rtrw").value
        //     var desa_kelurahan = document.getElementById("edit-kelurahan").value
        //     var kecamatan = document.getElementById("edit-kecamatan").value
        //     var agama = document.getElementById("edit-agama").value
        //     var status_kawin = document.getElementById("edit-status").value
        //     var pekerjaan = document.getElementById("edit-pekerjaan").value
        //     var warga = document.getElementById("edit-warga").value

        //     if (nama == "" || tempat_lahir == "" || tanggal_lahir == "" || jenis_kelamin == "" || alamat == "" || rt == "" || rw == "" || kelurahan == "" || kecamatan == "" || agama == "" || pekerjaan == "") {
        //         alert("data tidak boleh kosong")
        //     }else{
        //         var str_json = JSON.stringify({
        //           nik: nik,
        //           name: name,
        //           tempat_lahir: tempat_lahir,
        //           jeniskelamin: jeniskelamin,
        //           golongan_darah:golongan_darah,
        //           alamat: alamat,
        //           rtrw: rtrw,
        //           desa_kelurahan: desa_kelurahan,
        //           kecamatan: kecamatan,
        //           agama: agama,
        //           status_kawin: status_kawin,
        //           pekerjaan: pekerjaan,
        //           warga: warga
        //         })

        //         fetch('http://localhost:1414/update', {
        //                 method: 'PUT',
        //                 headers: {
        //                     'Content-Type': 'application/json',
        //                 },
        //                 body: str_json,
        //             })
        //             .then(response => response.json())
        //             .then(data => {
        //                 console.log('Success:', data);
        //                 window.location.href = "index2.html"
        //             })
        //             .catch((error) => {
        //                 console.error('Error:', error);
        //             });
        //     }
        // }
        //   function editData(data) {
        //     var updateModal = document.getElementById("updateModal")
        //     updateModal.innerHTML = `<div class="modal-dialog">
        //                                 <div class="modal-content">
        //                                     <div class="modal-header">
        //                                         <h5 class="modal-title">Edit Data Employee</h5>
        //                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        //                                             <span aria-hidden="true">&times;</span>
        //                                         </button>
        //                                     </div>
        //                                     <div class="modal-body">
        //                                         <input type="hidden" id="id" value="${data.id}">
        //                                         <div class="form-group">
        //                                             <label for="edit-nik">NIK</label>
        //                                             <input type="number" class="form-control" id="edit-nik" value="${data.name}" readonly>
        //                                         </div>
        //                                         <div class="form-group">
        //                                             <label for="edit-nama">Nama</label>
        //                                             <input type="text" class="form-control" id="edit-name" value="${data.nama}">
        //                                         </div>
        //                                         <div class="form-group">
        //                                             <div class="row">
        //                                                 <div class="col">
        //                                                     <label for="edit-tempat-lahir">Tempat Lahir</label>
        //                                                     <input type="text" class="form-control" id="edit-tempat-lahir" value="${data.tempat_lahir}">
        //                                                 </div>
        //                                               </div>
        //                                         </div>
        //                                         <div class="form-group">
        //                                             <label for="edit-jenis-kelamin">Jenis Kelamin</label>
        //                                             <select class="form-control" id="edit-jenis-kelamin">
        //                                                 <option value="${data.jeniskelamin}">${data.jeniskelamin}</option>
        //                                                 <option value="Pria">Pria</option>
        //                                                 <option value="Wanita">Wanita</option>
        //                                             </select>
        //                                         </div>
        //                                         <div class="form-group">
        //                                             <label for="edit-alamat">Alamat</label>
        //                                             <input type="text" class="form-control" id="edit-alamat" value="${data.alamat}">
        //                                         </div>
        //                                         <div class="form-group">
        //                                             <div class="row">
        //                                                 <div class="col">
        //                                                     <label for="edit-rt">RT</label>
        //                                                     <input type="number" class="form-control" id="edit-rt" value="${data.rtrw}">
        //                                                 </div>
        //                                             </div>
        //                                         </div>
        //                                         <div class="form-group">
        //                                             <div class="row">
        //                                                 <div class="col">
        //                                                     <label for="edit-kelurahan">Kelurahan</label>
        //                                                     <input type="text" class="form-control" id="edit-kelurahan" value="${data.desa_kelurahan}">
        //                                                 </div>
        //                                                 <div class="col">
        //                                                     <label for="edit-kecamatan">Kecamatan</label>
        //                                                     <input type="text" class="form-control" id="edit-kecamatan" value="${data.kecamatan}">
        //                                                 </div>
        //                                             </div>
        //                                         </div>
        //                                         <div class="form-group">
        //                                             <label for="edit-agama">Agama</label>
        //                                             <select class="form-control" id="edit-agama">
        //                                                 <option value="${data.agama}">${data.agama}</option>
        //                                                 <option value="Islam">Islam</option>
        //                                                 <option value="Protestan">Protestan</option>
        //                                                 <option value="Katolik">Katolik</option>
        //                                                 <option value="Hindu">Hindu</option>
        //                                                 <option value="Buddha">Buddha</option>
        //                                                 <option value="Khonghucu">Khonghucu</option>
        //                                             </select>
        //                                         </div>
        //                                         <div class="form-group">
        //                                             <label for="edit-status">Status Kawin</label>
        //                                             <select class="form-control" id="edit-status">
        //                                                 <option value="${data.status_kawin}">${data.status_kawin}</option>
        //                                                 <option value="Sudah Menikah">Sudah Menikah</option>
        //                                                 <option value="Belum Menikah">Belum Menikah</option>
        //                                             </select>
        //                                         </div>
        //                                         <div class="form-group">
        //                                             <label for="edit-pekerjaan">Pekerjaan</label>
        //                                             <input type="text" class="form-control" id="edit-pekerjaan" value="${data.pekerjaan}">
        //                                         </div>
        //                                         <div class="form-group">
        //                                             <label for="edit-warga">Kewarganegaraan</label>
        //                                             <select class="form-control" id="edit-warga">
        //                                                 <option value="${data.warga}">${data.warga}</option>
        //                                                 <option value="WNI">WNI</option>
        //                                                 <option value="WNA">WNA</option>
        //                                             </select>
        //                                         </div>
        //                                         <br>
        //                                         </div>
        //                                         <div class="modal-footer">
        //                                             <button type="submit" class="btn btn-primary" id="submit" onclick="updateData()">Submit</button>
        //                                         </div>
        //                                     </div>
        //                                 </div>`
        // }
    }


    http.send()
}

getData("http://localhost:1414/karyawans")