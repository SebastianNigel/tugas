function addData() {
    let nik = document.getElementById("nik").value;
    let name = document.getElementById("nama").value;
    let tempat_lahir = document.getElementById("tempat-tgl-lahir").value;
    let jeniskelamin = document.getElementById("jenis_kelamin").value;
    let golongan_darah = document.getElementById("golongan_darah").value;
    let alamat = document.getElementById("Alamat").value;
    let rtrw = document.getElementById("rtrw").value;
    let desa_kelurahan = document.getElementById("Kelurahan").value;
    let kecamatan = document.getElementById("Kecamatan").value;
    let agama = document.getElementById("agama").value;
    let status_kawin = document.getElementById("status_kawin").value;
    let pekerjaan = document.getElementById("Pekerjaan").value;
    let warga = document.getElementById("warga").value;

    let jsonStr = JSON.stringify({
        nik: nik,
        name: name,
        tempat_lahir: tempat_lahir,
        jeniskelamin: jeniskelamin,
        golongan_darah: golongan_darah,
        alamat: alamat,
        rtrw: rtrw,
        desa_kelurahan: desa_kelurahan,
        kecamatan: kecamatan,
        agama: agama,
        status_kawin: status_kawin,
        pekerjaan: pekerjaan,
        warga: warga
    });

    fetch('http://localhost:1414/addKaryawan', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: jsonStr,
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            window.location.href = "index2.html"
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    getData("http://localhost:1414/karyawans")
}

function deleteData(nik) {
    let del = confirm("Anda serius ingin menghapus data?")
    if (del) {
        fetch(`http://localhost:1414/deleteK/${nik}`, {
            method: 'DELETE',
        })
            .then(() => location.reload())
            .catch(console.error)
    }
}

function getData(url) {
    let http = new XMLHttpRequest();
    http.open("GET", url, true)
    http.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let res = JSON.parse(this.responseText)
            console.log(res.length)

            let msgReceipt = ""
            let no = 1;
            for (let i = 0; i < res.length; i++) {
                msgReceipt += `
<tr>
                    <td>${no}</td>
                    <td>${res[i].nik}</td>
                    <td>${res[i].name}</td>
                    <td>${res[i].tempat_lahir}</td>
                    <td>${res[i].jeniskelamin}</td>
                    <td>${res[i].golongan_darah}</td>
                    <td>${res[i].alamat}</td>
                    <td>
                    
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#${res[i].name}Detail" >
                    DETAIL
                </button>

                <div class="modal fade" id="${res[i].name}Detail" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">DETAIL DATA EMPLOYEE</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form>
                                <div class="form-group">
                                        <label style="font-weight: bold" for="nik">NIK</label>
                                        <br>
                                        ${res[i].nik}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="name">Nama</label>
                                        <br>
                                        ${res[i].name}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="tempat_lahir">Tempat Tanggal Lahir</label>
                                        <br>
                                        ${res[i].tempat_lahir}
                                    </div>                                 
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="jenis_kelamin">Jenis Kelamin</label>
                                        <br>
                                       ${res[i].jeniskelamin}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="golongan_darah">Golongan Darah</label>
                                        <br>
                                        ${res[i].golongan_darah}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="alamat">Alamat</label>
                                        <br>
                                        ${res[i].alamat}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="rtrw">Rt / Rw</label>
                                        <br>
                                        ${res[i].rtrw}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="kelurahan">Kelurahan</label>
                                        <br>
                                        ${res[i].desa_kelurahan}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="kecamatan">Kecamatan</label>
                                        <br>
                                        ${res[i].kecamatan}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="agama">Agama</label>
                                        <br>
                                        ${res[i].agama}
                                    </div>
                                    <label style="font-weight: bold" for="status_perkawinan">Status Perkawinan</label>
                                    <br>
                                    <div class="form-group">
                                        ${res[i].status_kawin}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="pekerjaan">Pekerjaan</label>
                                        <br>
                                        ${res[i].pekerjaan}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="kewarganegaraan">Kewarganegaraan</label>
                                        <br>
                                        ${res[i].warga}
                                    </div>

                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>



                     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#${res[i].name}Update">
                    UPDATE
                </button>

                <div class="modal fade" id="${res[i].name}Update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Update Data</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
<input type="hidden" value="${res[i].nik}" id="nik">
                                    
                    <form style="font-weight: bold">  
                                    <div class="form-group">
                                        <label for="name">Nama</label>
                                        <input type="text" class="form-control" id="edit-name" value="${res[i].name}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="tempat_lahir">Tempat Tanggal Lahir</label>
                                        <input type="text" class="form-control" id="edit-tempat_lahir" value="${res[i].tempat_lahir}" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="jenis_kelamin">Jenis Kelamin</label>
                                        <select class="custom-select" id="edit-jenis_kelamin">
                                            <option value="${res[i].jeniskelamin}">${res[i].jeniskelamin}</option>
                                            <option value="Laki-Laki">Laki-Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="golongan_darah">Golongan Darah</label>
                                        <select class="custom-select" id="edit-golongan_darah">
                                            <option value="${res[i].golongan_darah}">${res[i].golongan_darah}</option>
                                            <option value="A"">A</option>
                                            <option value="B"">B</option>
                                            <option value="AB"">AB</option>
                                            <option value="O"">O</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">Alamat</label>
                                        <input type="text" class="form-control" id="edit-alamat" value="${res[i].alamat}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="rtrw">Rt / Rw</label>
                                        <input type="text" class="form-control" id="edit-rtrw" value="${res[i].rtrw}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="kelurahan">Kelurahan</label>
                                        <input type="text" class="form-control" id="edit-kelurahan" value="${res[i].desa_kelurahan}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="kecamatan">Kecamatan</label>
                                        <input type="text" class="form-control" id="edit-kecamatan" value="${res[i].kecamatan}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="agama">Agama</label>
                                        <select class="custom-select" id="edit-agama">
                                            <option value="${res[i].agama}">${res[i].agama}</option>
                                            <option value="Buddha">Buddha</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Islam">Islam</option>
                                            <option value="Katholik">Katholik</option>
                                            <option value="Khong Hu Cu">Khong Hu Cu</option>
                                            <option value="Kristen">Kristen</option>
                                        </select>
                                    </div>
                                    <label for="status_perkawinan">Status Perkawinan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="edit-status_kawin" >
                                            <option value="${res[i].status_kawin}">${res[i].status_kawin}</option>
                                            <option value="Belum Kawin">Belum Kawin</option>
                                            <option value="Sudah Kawin">Sudah Kawin</option>
                                            <option value="Sudah Pernah Kawin">Sudah Pernah Kawin</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="pekerjaan">Pekerjaan</label>
                                        <input type="text" class="form-control" id="edit-pekerjaan" value="${res[i].pekerjaan}" />
                                    </div>

                                    <div class="form-group">
                                        <label for="kewarganegaraan">Kewarganegaraan</label>
                                        <select class="custom-select" id="edit-warga">
                                            <option value="${res[i].warga}">${res[i].warga}</option>
                                            <option value="WNI">WNI</option>
                                            <option value="WNA">WNA</option>
                                        </select>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                            
                                        <button type="submit" class="btn btn-primary" onclick="updateData(${res[i].nik})">Update Data</button>
                                    </div>

                            </div>
                    </form>
                        </div>
                    </div>
                </div>


                    <button type="button" class="btn btn-danger" onclick="deleteData(${res[i].nik})">DELETE</button>
</td>
</tr>
                `
                no++;
            }
            document.getElementById("tableBody").innerHTML = msgReceipt
        }
    }
    http.send()
}

function updateData() {
    let nik = document.getElementById("nik").value;
    let name = document.getElementById("edit-name").value;
    let tempat_lahir = document.getElementById("edit-tempat_lahir").value;
    let jeniskelamin = document.getElementById("edit-jenis_kelamin").value;
    let golongan_darah = document.getElementById("edit-golongan_darah").value;
    let alamat = document.getElementById("edit-alamat").value;
    let rtrw = document.getElementById("edit-rtrw").value;
    let desa_kelurahan = document.getElementById("edit-kelurahan").value;
    let kecamatan = document.getElementById("edit-kecamatan").value;
    let agama = document.getElementById("edit-agama").value;
    let status_kawin = document.getElementById("edit-status_kawin").value;
    let pekerjaan = document.getElementById("edit-pekerjaan").value;
    let warga = document.getElementById("edit-warga").value;

    let jsonStr = JSON.stringify({
        nik : nik,
        name: name,
        tempat_lahir: tempat_lahir,
        jeniskelamin: jeniskelamin,
        golongan_darah: golongan_darah,
        alamat: alamat,
        rtrw: rtrw,
        desa_kelurahan: desa_kelurahan,
        kecamatan: kecamatan,
        agama: agama,
        status_kawin: status_kawin,
        pekerjaan: pekerjaan,
        warga: warga
    });


    fetch('http://localhost:1414/update', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: jsonStr,
    })
        .then(response => response.json())
        .then(() => {
            console.log(jsonStr)
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

getData("http://localhost:1414/karyawans")