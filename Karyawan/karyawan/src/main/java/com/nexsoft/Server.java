package com.nexsoft;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.sql.SQLException;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

public class Server extends WebSocketServer {
    public Server(int port) {
        super(new InetSocketAddress(port));
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        int port = 8989;

        try {
            port = Integer.parseInt(args[0]);
        } catch (Exception e) {
        }

        Server serverEmployee = new Server(port);
        serverEmployee.start();
        System.out.println("Port Server ini " + serverEmployee.getPort());

        BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String in = sysin.readLine();
            serverEmployee.broadcast(in);
            if (in.equals("exit")) {
                serverEmployee.stop(1000);
                break;
            }
        }
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        System.out.println("Sudah Masuk !");
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("Keluar !");
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        Karyawan employee = new Karyawan();
        try {
            employee.addData(message);
            for (String data : employee.getDatas()) {
//                broadcast(data);
                System.out.println(data);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        ex.printStackTrace();
        if (conn != null) {

        }
    }

    @Override
    public void onStart() {
        System.out.println("Server Run !");
    }
}