package com.nexsoft;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import org.json.JSONObject;

public class Karyawan {
    Connection connection = new ConnectionDB().Connect();
    ArrayList<String> datas = new ArrayList<>();

    void showData() throws SQLException {
        String query = "SELECT * FROM ktp";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery(query);
        while (resultSet.next()) {
            String nik = resultSet.getString("nik");
            String nama = resultSet.getString("nama");
            String ttlahir = resultSet.getString("tempat_lahir");
            String jkel = resultSet.getString("jenis_kel");
            String alamat = resultSet.getString("alamat");
            String agama = resultSet.getString("agama");
            String kawin = resultSet.getString("status_kawin");
            String kerja = resultSet.getString("pekerjaan");
            String warga = resultSet.getString("warga");

            String response = "{\"nik\" : \"" + nik + "\", \"nama\" : \"" + nama + "\", \"tempat&tanggal_lahir\" : \"" + ttlahir + "\", \"jenis_kelamin\" : \"" + jkel + "\",\"alamat\" : \"" + alamat + "\",\"agama\" : \"" + agama + "\",\"status_kawin\" : \"" + kawin + "\",\"pekerjaan\" : \"" + kerja + "\",\"kewarganegaraan\" : \"" + warga + "\"}";
//            System.out.println(response);
            datas.add(response);
        }
    }

    void addData(String theJSON) throws SQLException {
        JSONObject theResponse = new JSONObject(theJSON);
        if (theResponse.getString("type").equalsIgnoreCase("Data")) {
            String nik = theResponse.getString("nik");
            String nama = theResponse.getString("nama");
            String ttlahir = theResponse.getString("tempat_lahir");
            String jkel = theResponse.getString("jenis_kel");
            String alamat = theResponse.getString("alamat");
            String agama = theResponse.getString("agama");
            String kawin = theResponse.getString("status_kawin");
            String kerja = theResponse.getString("pekerjaan");
            String warga = theResponse.getString("warga");

            String query = " INSERT INTO ktp (nik,nama,tempat_lahir,jenis_kel,alamat,agama,status_kawin,pekerjaan,warga) VALUES('" + nik + "','" + nama + "','" + ttlahir + "','" + jkel + "','" + alamat + "','" + agama + "','" + kawin + "','" + kerja + "','" + warga + "')";

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.executeUpdate();
        }
    }

    ArrayList<String> getDatas() throws SQLException {
        showData();
        return datas;
    }
}