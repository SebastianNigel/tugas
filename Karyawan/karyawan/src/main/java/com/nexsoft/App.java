package com.nexsoft;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        try {
            System.out.print("Masukkan Nik : ");
            Scanner nik = new Scanner(System.in);
            String hasilNik = nik.nextLine();

            System.out.print("Masukkan Nama : ");
            Scanner nama = new Scanner(System.in);
            String hasilNama = nama.nextLine();

            System.out.print("Masukkan Tempat&Tanggal Lahir (cth: Tangerang, 30-06-2000) : ");
            Scanner ttlahir = new Scanner(System.in);
            String hasilLahir = ttlahir.nextLine();

            System.out.print("Masukkan Jenis Kelamin (cth: L/P) : ");
            Scanner jkel = new Scanner(System.in);
            String hasilJenis = jkel.nextLine();
            if (hasilJenis.equalsIgnoreCase("l")) {
                hasilJenis = "Laki-Laki";
            } else if (hasilJenis.equalsIgnoreCase("p")) {
                hasilJenis = "Perempuan";
            }

            System.out.print("Masukkan Alamat : ");
            Scanner alamat = new Scanner(System.in);
            String hasilAlamat = alamat.nextLine();

            System.out.print("Masukkan Agama : ");
            Scanner agama = new Scanner(System.in);
            String hasilAgama = agama.nextLine();

            System.out.print("Masukkan Status Perkawinan (cth: Belum Kawin/Kawin) : ");
            Scanner kawin = new Scanner(System.in);
            String hasilKawin = kawin.nextLine();
            if (hasilKawin.equalsIgnoreCase("Belum")) {
                hasilKawin = "Belum Kawin";
            }
            if (hasilKawin.equalsIgnoreCase("Belum Kawin")) {
                hasilKawin = "Belum Kawin";
            } else {
                hasilKawin = "Kawin";
            }

            System.out.print("Masukkan Pekerjaan : ");
            Scanner kerja = new Scanner(System.in);
            String hasilKerja = kerja.nextLine();

            System.out.print("Masukkan Kewarganegaraan : ");
            Scanner warga = new Scanner(System.in);
            String hasilWarga = warga.nextLine();
            if (hasilWarga.equalsIgnoreCase("wni")) {
                hasilWarga = "WNI";
            } else {
                hasilWarga = "WNA";
            }
            nik.close();
            nama.close();
            ttlahir.close();
            alamat.close();
            agama.close();
            jkel.close();
            kawin.close();
            kerja.close();
            warga.close();

            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/karyawan?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");

            String sql = " INSERT INTO ktp (nik,nama,tempat_lahir,jenis_kel,alamat,agama,status_kawin,pekerjaan,warga) VALUES('" + hasilNik + "','" + hasilNama + "','" + hasilLahir + "','" + hasilJenis + "','" + hasilAlamat + "','" + hasilAgama + "','" + hasilKawin + "','" + hasilKerja + "','" + hasilWarga + "')";
            PreparedStatement prest = myConn.prepareStatement(sql);
            prest.execute();
            System.out.println(" Input Data Berhasil ");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}