package com.nexsoft;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionDB {
    static String db_url = "jdbc:mysql://localhost:3306/karyawan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    Connection connection;

    public Connection Connect() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(db_url, "root", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}